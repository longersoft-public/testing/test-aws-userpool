import { CognitoUserPool } from 'amazon-cognito-identity-js'

const poolData = {
    UserPoolId: 'ap-southeast-1_mf2Q4NT0s',
    ClientId: '55qtfvqndo2605cc7gf0phutp2'
};

export default new CognitoUserPool(poolData);
